import React from "react";
import { StyleSheet, SafeAreaView, Text, View, Pressable, TextInput } from 'react-native';
import Button from "../../components/button";


const LoginScreen = ({ navigation }) => {
    return (
        <View style={styles.main}>
            <View style={styles.container}>
                <Text style={styles.text} >Usuário</Text>
                <TextInput style={styles.input}></TextInput>
                <Text style={styles.text}>Senha</Text>
                <TextInput style={styles.input} secureTextEntry={true}></TextInput>
                <Button
                    title="Entrar"
                    onPress={() =>
                        navigation.navigate('Home')
                    }
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: 'black'
    },
    container: {
        flex: 1,
        padding: 15,
        marginTop: '50%'
    },
    input: {
        height: 50,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: 'white',
        color: '#FFFF00'
    },
    text: {
        marginTop: 15,
        marginBottom: 15,
        color: '#FFFF00',
        fontSize: 20,
        fontWeight: "bold",
    },
    button: {
        marginTop: 10
    }
});

export default LoginScreen;
