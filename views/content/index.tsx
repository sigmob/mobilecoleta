import React from "react";
import { StyleSheet, SafeAreaView, Text, View } from 'react-native';
import Button from "../../components/button";

const ContentScreen = ({ navigation, route }) => {
    return (
        
        <View style={styles.container}>
            <Text style={styles.textHeader}>INFORMAÇÕES DA COLETA</Text>

            <Text style={styles.text}>Status:</Text>
            <Text style={styles.text}>Previsão:</Text>
            <Text style={styles.text}>Atualização:</Text>
            <Text style={styles.text}>Perídodo da coleta:</Text>
            <Text style={styles.text}>Dias da semana:</Text>


            <Button
                title="Ver no mapa"
                onPress={() =>
                    navigation.navigate('Map')
                }
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 15
    },

    textHeader: {
        fontSize: 20,
        fontWeight: "bold",
        marginTop: 15,
        marginBottom: 40,
        textAlign: "center",
    },

    text: {
        fontSize: 20,
        fontWeight: "bold",
        marginTop: 15,
        marginBottom: 15
    },

});

export default ContentScreen;
