import React, { useState } from "react";
import { useEffect } from "react";
import { StyleSheet, SafeAreaView, Text, View, StatusBar, Alert, TextInput, ActivityIndicator } from 'react-native';
import { AutocompleteDropdown } from "react-native-autocomplete-dropdown";
import Button from "../../components/button";
import { default as listNeighborhood } from '../../data/locais.json'

const e = console.log;


const HomeScreen = ({ navigation }) => {

    const [neighborhood, setneighborhood] = useState<any>([{ id: '0', title: '' }]);
    const [locals, setLocals] = useState<any>([{ id: '0', title: '' }]);
    const [selectedItem, setSelectedItem] = useState({});
    const [selectedLocal, setSelectedLocal] = useState({});
    const [isloading, setIsLoading] = useState(false);

    useEffect(() => {
        const arr: string[] = [];

        listNeighborhood.forEach(x => {
            let index = arr.findIndex(a => a === x.bairro);
            if (index === -1) {
                arr.push(x.bairro);
            }
        })

        arr.map((bairro: string, index: number) => {
            setneighborhood((oldArray: any) => [...oldArray, { title: bairro, id: index + 1 }])
        });


    }, [])


    useEffect(() => {

        if (selectedItem === 1000) {
            locals.map((x, i) => {
                setLocals(locals.slice(i, 1))
            });

            if (selectedLocal !== 0 && selectedItem === 1000) {
                e(`Item ${selectedItem}`);
                Alert.alert('Atenção', 'Limpe os endereços clicando no "x" para continuar', [
                    { text: "OK" }
                ]);
            }
        }

        onchangeBairro(Number(selectedItem));

    }, [selectedItem])


    useEffect(() => {

        e(selectedLocal);

    }, [selectedLocal])


    const onchangeBairro = (item: number) => {


        if (isNaN(item)) {
            return;
        }

        let filtered = neighborhood.filter(x => x.id === item);

        if (filtered.length === 0) {
            return;
        }

        const ng = filtered[0].title;

        filtered = listNeighborhood.filter(x => x.bairro === ng);

        e(filtered);

        const arr: string[] = [];

        filtered.forEach(x => {
            let index = arr.findIndex(a => a === x.local);
            if (index === -1) {
                arr.push(x.local);
            }
        })

        arr.map((local: string, index: number) => {
            e(local, index);
            setLocals((oldArray: any) => [...oldArray, { title: local, id: index + 1 }])
        });


    }

    return (
        <View style={[styles.container, {
            // Try setting `flexDirection` to `"row"`.
            flexDirection: "column"
        }]}>
            <View style={{ flex: .2 }}>
                <SafeAreaView>
                    <StatusBar />
                </SafeAreaView>
            </View>
            <View style={{ flex: 1 }}>
                <Text style={styles.text}>Bairro</Text>

                <AutocompleteDropdown
                    inputContainerStyle={styles.inputText}
                    clearOnFocus={true}
                    closeOnBlur={false}
                    closeOnSubmit={false}
                    onClear={() => setSelectedItem(1000)}
                    suggestionsListContainerStyle={{ backgroundColor: 'white' }}
                    suggestionsListTextStyle={{ marginLeft: 5 }}
                    initialValue={{ id: '0' }} // or just '2'
                    onSelectItem={(item: any) => {
                        item && setSelectedItem(item.id);
                    }}
                    dataSet={neighborhood}
                />

                <Text style={styles.text}>Endereços</Text>

                <AutocompleteDropdown
                    inputContainerStyle={styles.inputText}
                    clearOnFocus={true}
                    closeOnBlur={false}
                    closeOnSubmit={false}
                    suggestionsListContainerStyle={{ backgroundColor: 'white' }}
                    suggestionsListTextStyle={{ marginLeft: 5 }}
                    initialValue={{ id: '0' }} // or just '2'
                    onSelectItem={(item: any) => {
                        item && setSelectedLocal(item.id);
                    }}
                    onClear={() => setSelectedLocal(0)}
                    dataSet={locals}
                />

                <Text style={styles.text}>Número</Text>
                <TextInput style={styles.input} keyboardType='numeric'></TextInput>

                <Button
                    title="Verificar"
                    onPress=
                    {
                        () => {
                            setIsLoading(true);
                            setTimeout(() => {
                                setIsLoading(false);
                                navigation.navigate('Content')
                            }, 2000)
                        }
                    }
                />

                {isloading &&
                    (<ActivityIndicator size={100} color="#0000ff" style={styles.spinner} />)
                }


            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 0.5,
        padding: 15
    },

    text: {
        fontSize: 20,
        fontWeight: "bold",
        marginTop: 15,
        marginBottom: 15
    },

    inputText: {
        borderRadius: 10,
        borderWidth: 1,
        height: 50
    },

    input: {
        height: 50,
        borderWidth: 1,
        borderRadius: 10,
        fontSize: 20,
        padding: 5

    },
    spinner: {
        marginTop: 50,
    }


});




export default HomeScreen;
