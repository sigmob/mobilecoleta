import React from "react";
import { StyleSheet, View, Dimensions } from 'react-native';
import MapView, { Polyline } from 'react-native-maps';
import { Marker } from 'react-native-maps';
const width = Dimensions.get('screen').width;
const height = Dimensions.get('screen').height;




const mapScreen = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <MapView
                style={styles.mapStyle}
                region={{
                    latitude: -22.9111957,
                    longitude: -43.2078098,
                    latitudeDelta: 0.01,
                    longitudeDelta: 0.01 * (width / height),
                }}
                zoomEnabled={true}

            >
                <Marker
                    key={'1eee'}
                    coordinate={{ latitude: -22.9111957, longitude: -43.2078098 }}
                    icon={require('../../assets/car3.png')}


                ></Marker>

                <Marker
                    key={'3ww'}
                    coordinate={{ latitude: -22.910222, longitude: -43.204806 }}
                ></Marker>

                <Polyline
                    key={'2qqq'}
                    coordinates={[
                        { latitude: -22.911108, longitude: -43.207839 },
                        { latitude: -22.910852, longitude: -43.206938 },
                        { latitude: -22.910768, longitude: -43.206635 },
                        { latitude: -22.910628, longitude: -43.206165 },
                        { latitude: -22.910554, longitude: -43.205888 },
                        { latitude: -22.910431, longitude: -43.205577 },
                        { latitude: -22.910412, longitude: -43.205506 },
                        { latitude: -22.910339, longitude: -43.205275 },
                        { latitude: -22.910228, longitude: -43.204907 },
                        { latitude: -22.91022, longitude: -43.204855 },
                        { latitude: -22.910222, longitude: -43.204806 }
                    ]}
                    strokeColor="#8B0000"
                    strokeWidth={2.5}
                />
            </MapView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 15,
    },
    mapStyle: {
        width: Dimensions.get('window').width * .92,
        height: '100%'
    }
});

export default mapScreen;
